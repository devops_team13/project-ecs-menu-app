from flask import Flask, render_template, request

app = Flask(__name__)

# Define the menu items
menu_items = [
    {'id': 1, 'name': 'Pizza'},
    {'id': 2, 'name': 'Burger'},
    {'id': 3, 'name': 'Pasta'},
    {'id': 4, 'name': 'Salad'}
]

@app.route('/')
def index():
    return render_template('index.html', menu_items=menu_items)

@app.route('/submit', methods=['POST'])
def submit():
    selected_items = request.form.getlist('items')
    return render_template('success.html', selected_items=selected_items)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=80,debug=True)