# Use the official Python 3.9 slim image from the Docker Hub
FROM python:3.9-slim

# Set the working directory inside the container
WORKDIR /menuapp

# Copy the requirements file into the container
COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code into the container
COPY . .

# Expose port 80 to the outside world
EXPOSE 80

# Run the application
CMD ["python", "app.py"]
